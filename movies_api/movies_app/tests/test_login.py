import json
from django.urls import reverse
from rest_framework.test import APITestCase
from django.contrib.auth.models import User
from rest_framework.authtoken.models import Token
from rest_framework import status
from django.contrib.auth.models import User
from rest_framework import status
from rest_framework.reverse import reverse
from rest_framework.test import RequestsClient, APIRequestFactory
from rest_framework.test import APITestCase, APIClient

from movies_app.models  import Movie


def create_user(self):
        self.client = APIClient()
        self.user = User(email='testuser@example.com',username='testuser1',password="testpassword")
        self.user.set_password("124testpassword")
        self.user.save()
        self.token = Token.objects.create(user=self.user)
    

class TestLoginView(APITestCase):
    
    def test_request_client(self):
        create_user(self)
        data = {"username":"testuser1","password":"124testpassword","email":"testuser@example.com"}
        data = json.dumps(dict(data))
        response = self.client.post("/api/login/", data, content_type='application/json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        data = {"username":"testuser1","password":"testpass1","email":"testuser@example.com"}
        data = json.dumps(dict(data))
        response = self.client.post("/api/login/",data, content_type='application/json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        
    