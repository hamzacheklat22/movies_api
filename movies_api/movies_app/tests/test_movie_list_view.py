from django.urls import reverse
from rest_framework.test import APITestCase
from django.contrib.auth.models import User
from rest_framework.authtoken.models import Token
from rest_framework import status
from django.contrib.auth.models import User
from rest_framework import status
from rest_framework.reverse import reverse
from rest_framework.test import RequestsClient, APIRequestFactory
from rest_framework.test import APITestCase, APIClient

from movies_app.models  import Movie


def create_user(self):
        self.client = APIClient()
        self.user = User(email='test@gmail.com',username='test',password="abcdef123")
        self.user.set_password("abc123")
        self.user.save()
        self.token = Token.objects.create(user=self.user)
    
    
class TestMovieListView(APITestCase):
    
    def setUp(self):
        
        Movie.objects.create(movie_id=1, title='abc')
        Movie.objects.create(movie_id=2, title='def')

    def test_request_client(self):
        
        create_user(self)
        response = self.client.get('/api/')
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)
        self.client.credentials(HTTP_AUTHORIZATION='Token ' + self.token.key)
        response = self.client.get('/api/')
        self.assertEqual(response.status_code, 200)

    def test_number_of_movies(self):
        self.assertEqual(Movie.objects.count(), 2)
        

