from django.db import models


class Movie(models.Model):

    movie_id = models.CharField(max_length=255, unique=True, primary_key=True,null=False)
    title = models.CharField(max_length=255)
    poster = models.URLField(null=True)
   
    def __str__(self):
        return self.title