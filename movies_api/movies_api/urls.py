"""
movies_api URL Configuration

"""
from django.contrib import admin
from django.urls import path
from django.conf.urls import url,include  


urlpatterns = [
    path('admin/', admin.site.urls),
    path('', include('movies_app.urls')),
  
  
   
]
